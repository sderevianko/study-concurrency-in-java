package CompletableFutureStudy;

import java.util.Scanner;
import java.util.concurrent.CompletableFuture;

public class Study_05_exceptionally {

    public static final String UNKNOWN = "unknown";

    public static void main(String... args) {
        try (Scanner in = new Scanner(System.in)) {
            System.out.print("Enter age of person:");
            int age = Integer.parseInt(in.nextLine().trim());

            CompletableFuture<String> future;
            future = CompletableFuture.supplyAsync(() -> {
                if (age < 0) throw new RuntimeException("Age can't be negative");
                System.out.println("Check for Child...");
                if(age == 14) throw new RuntimeException("The uncertain age");
                return age < 14 ? "Child" : UNKNOWN;
            })
                    .thenApplyAsync(a -> {
                        System.out.println("Check for Junior...");
                        if (!a.equals(UNKNOWN)) return a;
                        if(age == 18) throw new RuntimeException("The uncertain age");
                        return age < 18 ? "Junior" : UNKNOWN;
                    })
                    .thenApply(a -> {
                        System.out.println("Check for Major...");
                        if (!a.equals(UNKNOWN)) return a;
                        if(age == 25) throw new RuntimeException("The uncertain age");
                        return age < 25 ? "Major" : UNKNOWN;
                    })
                    .thenApplyAsync(a -> {
                        System.out.println("Check for Adult...");
                        if (!a.equals(UNKNOWN)) return a;
                        if(age == 75) throw new RuntimeException("The uncertain age");
                        return age < 75 ? "Adult" : UNKNOWN;
                    })
                    .thenApply(a -> {
                        System.out.println("Check for Elderly...");
                        if (!a.equals(UNKNOWN)) return a;
                        if(age <= 120) return "Elderly";
                        throw new RuntimeException("Age can't be greater then 120");
                    })
//                    .exceptionally(e -> {
//                        System.out.printf("Interruped with exception %s%n", e.getLocalizedMessage());
//                        return UNKNOWN;
//                    });
                    .handle((a,e) -> {
                        if(e != null) {
                            System.out.printf("Interruped with exception %s%n", e.getLocalizedMessage());
                            return UNKNOWN;
                        }
                        return a;
                    });

            System.out.printf("The status of person with age %d is \"%s\"%n", age, future.join());

        } catch (NumberFormatException e) {
            System.out.println("Wrong number of age");
        }
    }

}
