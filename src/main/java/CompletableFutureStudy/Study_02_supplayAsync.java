package CompletableFutureStudy;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Study_02_supplayAsync {
    public static void main(String... args) throws ExecutionException, InterruptedException {
        System.out.println("Start computations...");

        CompletableFuture<Integer> i_result = CompletableFuture.supplyAsync(() -> {
            int i;
            for (i = 0; i < 10; i++) {
                try {
                    Thread.sleep(500);
                    System.out.printf("got i = %d...%n", i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return i;
        }).thenApply((i) -> {
            System.out.printf("Finally we've got i = %d%n", i);
            return i;
        });

        CompletableFuture<Integer> j_result = CompletableFuture.supplyAsync(() -> {
            int j;
            for (j = 0; j < 10; j++) {
                try {
                    Thread.sleep(1000);
                    System.out.printf("\t\t\t\t\t...got j = %d%n", j);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return j;
        }).thenApply((j) -> {
            System.out.printf("Finally we've got j = %d%n", j);
            return j;
        });

//        CompletableFuture<Void> result = CompletableFuture.allOf(i_result, j_result)
//                .thenApply(a -> i_result.join() + j_result.join())
//                .thenAccept(r -> System.out.printf("Finally we've got i + j = %d%n", r));

        CompletableFuture<Void> result = CompletableFuture.anyOf(i_result, j_result)
                .thenApply(a -> getIfDone(i_result, "i") + getIfDone(j_result, "j"))
                .thenAccept(r -> System.out.printf("Finally we've got i + j = %d%n", r));

        result.join();
        System.out.println("Finished main thread!");
    }

    private static int getIfDone(CompletableFuture<Integer> futureValue, String name) {
        if (futureValue.isDone()) {
            System.out.printf("\"%s\" is done%n", name);
        } else {
            futureValue.complete(0);
            System.out.printf("\"%s\" is note done and completed with zero%n", name);
        }
        return futureValue.join();
    }
}
