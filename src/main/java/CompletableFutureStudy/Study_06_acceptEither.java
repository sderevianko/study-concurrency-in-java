package CompletableFutureStudy;

import java.util.concurrent.CompletableFuture;

public class Study_06_acceptEither {
    public static void main(String... args) {
        CompletableFuture<String> first = CompletableFuture.supplyAsync(() -> pingString("First", 1000));
        CompletableFuture<String> second = CompletableFuture.supplyAsync(() -> pingString("Second", 800));
        CompletableFuture<Void> result = first.acceptEither(second, s -> System.out.printf("Winner is %s%n",s));
        result.join();

        first = CompletableFuture.supplyAsync(() -> pingString("First", 1000));
        second = CompletableFuture.supplyAsync(() -> pingString("second", 1800));
        CompletableFuture<String> winner = first.applyToEitherAsync(second, s -> String.format("Winner is %s%n",s));
        System.out.println(winner.join());

        first = CompletableFuture.supplyAsync(() -> pingString("First", 1000));
        second = CompletableFuture.supplyAsync(() -> pingString("second", 800));
        result = first.runAfterBoth(second, ()->String.format("Both processes are completed"));
        result.join();

        first = CompletableFuture.supplyAsync(() -> pingString("First", 1000));
        second = CompletableFuture.supplyAsync(() -> pingString("second", 800));
        result = first.runAfterEither(second, ()->String.format("One process is already completed"));
        result.join();
    }

    private static String pingString(String string, int pause) {
        try {
            Thread.sleep(pause);
            return string;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return "Interrupted";
        }
    }

}
