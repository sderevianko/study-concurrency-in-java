package CompletableFutureStudy;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Study_03_thenCombine {
    public static void main(String... args) throws ExecutionException, InterruptedException {
        System.out.println("Start computations...");

        CompletableFuture<Integer> firstArg = CompletableFuture.supplyAsync(() -> getSum(15, 700, "First"))
                .thenApplyAsync(sum -> {
                    showResult(sum, 4000, "first sum");
                    return sum;
                });

        CompletableFuture<Void> result = CompletableFuture.supplyAsync(() -> getSum(11, 1200, "Second"))
                .thenApply(sum -> {
                    showResult(sum, 3000, "second sum");
                    return sum;
                }).thenCombine(firstArg, (a, b) -> a + b)
                .thenAcceptAsync(total -> {
                    showResult(total, 5000, "total sum");
                });

        result.join();
        System.out.println("Finished main thread!");
    }

    private static int getSum(int steps, int pause, String name) {
        int sum = 0;
        try {
            System.out.printf("%s starts...%n", name);
            for (int i = 0; i < 10; i++) {
                Thread.sleep(pause);
                int v = (int) (Math.random() * 1000);
                System.out.printf("%s: %d + %d = %d%n", name, sum, v, sum += v);
            }
            System.out.printf("%s finished%n", name);
            return sum;
        } catch (InterruptedException e) {
            System.out.println(e.getLocalizedMessage());
        }
        return sum;
    }

    private static void showResult(int sum, int pause, String name) {
        try {
            Thread.sleep(pause);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("And finally we've got %s = %d%n", name, sum);
    }
}
