package CompletableFutureStudy;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Study_01_runAsync {
    public static void main(String... args) throws ExecutionException, InterruptedException {
        System.out.println("Start computations...");

        CompletableFuture<Void> i_result = CompletableFuture.runAsync(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(500);
                    System.out.printf("got i = %d...%n",i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).thenRun(()->System.out.println("We've got all \"i\"!!!"));

        CompletableFuture<Void> j_result = CompletableFuture.runAsync(() -> {
            for (int j = 0; j < 10; j++) {
                try {
                    Thread.sleep(1000);
                    System.out.printf("\t\t\t\t\t...got j = %d%n",j);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).thenRun(()->System.out.println("We've got all \"j\"!!!"));

        CompletableFuture<Void> result = CompletableFuture.allOf(i_result, j_result);
        result.join();
        System.out.println("We've got all!");
    }
}
