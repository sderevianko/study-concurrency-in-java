package synchronization;

class FreeWorker extends Thread {

    private final String name;
    private final FreeResource from;
    private final FreeResource to;
    private final int power;

    FreeWorker(String name, int power, FreeResource from, FreeResource to) {
        this.name = name;
        this.power = power;
        this.from = from;
        this.to = to;
    }

    @Override
    public void run() {
        try {
            System.out.printf("%s starts working!!!%n", name);
            while (from.value != 0) {
                from.value--;
                System.out.printf("%s get %d%n", name, from.value);
                sleep(power);
                to.value++;
                System.out.printf("%s put %d%n", name, to.value);
            }
            System.out.printf("%s ends working!!!%n", name);
        } catch (InterruptedException e) {
        }
    }
}

class FreeResource {
    public int value;

    FreeResource(int value) {
        this.value = value;
    }
}

public class SynchFree {

    public static void main(String... args) {
        System.out.println("Study deadlock");
        FreeResource resource1 = new FreeResource(10);
        FreeResource resource2 = new FreeResource(10);
        FreeWorker worker1 = new FreeWorker("Jhon", 200, resource1, resource2);
        FreeWorker worker2 = new FreeWorker("Sam", 100, resource2, resource1);
        worker1.start();
        worker2.start();
    }
}
