package synchronization;

class RaceWorker extends Thread {

    private final String name;
    private final RaceResource from;
    private final RaceResource to;
    private final int power;

    RaceWorker(String name, int power, RaceResource from, RaceResource to) {
        this.name = name;
        this.power = power;
        this.from = from;
        this.to = to;
    }

    @Override
    public void run() {
        try {
            System.out.printf("%s starts working!!!%n", name);
            int value;
            while ((value = from.get()) != 0) {
                System.out.printf("%s get %d%n", name, value);
                sleep(power);
                value = to.put();
                System.out.printf("%s put %d%n", name, value);
            }
            System.out.printf("%s ends working!!!%n", name);
        } catch (InterruptedException e) {
        }
    }
}

class RaceResource {
    private int value;

    RaceResource(int value) {
        this.value = value;
    }

    public synchronized int get() {
        return value > 0 ? value-- : 0;
    }

    public synchronized int put() {
        return ++value;
    }
}

public class RaceStudy {

    public static void main(String... args) {
        System.out.println("Study deadlock solution");
        RaceResource resource1 = new RaceResource(10);
        RaceResource resource2 = new RaceResource(10);
        RaceWorker worker1 = new RaceWorker("Jhon", 400, resource1, resource2);
        RaceWorker worker2 = new RaceWorker("Sam", 500, resource1, resource2);
        worker1.start();
        worker2.start();
    }
}
