package synchronization;

class Worker extends Thread {

    private final String name;
    private final Resource from;
    private final Resource to;
    private final int power;

    Worker(String name, int power, Resource from, Resource to) {
        this.name = name;
        this.power = power;
        this.from = from;
        this.to = to;
    }

    @Override
    public void run() {
        try {
            System.out.printf("%s starts working!!!%n", name);
            while (from.value != 0) {
                synchronized (from) {
                    from.value--;
                    System.out.printf("%s get %d%n", name, from.value);
                    sleep(power);
                    synchronized (to) {
                        to.value++;
                        System.out.printf("%s put %d%n", name, to.value);
                    }
                }
            }
            System.out.printf("%s ends working!!!%n", name);
        } catch (InterruptedException e) {
        }
    }
}

class Resource {
    public int value;

    Resource(int value) {
        this.value = value;
    }
}

public class DeadlockStudy {

    public static void main(String... args) {
        System.out.println("Study deadlock");
        Resource resource1 = new Resource(10);
        Resource resource2 = new Resource(10);
        Worker worker1 = new Worker("Jhon", 200, resource1, resource2);
        Worker worker2 = new Worker("Sam", 100, resource2, resource1);
        worker1.start();
        worker2.start();
    }
}
